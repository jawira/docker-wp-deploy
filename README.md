WP Deploy
=========

This Dockerfile contains all required dependencies to deploy a WordPress site.

Contains:

- git
- mysql-client-5.7
- rsync
- ssh
- subversion

It should inherit:

- make
- PHP 7.2
- wget


Links:

- Bitbucket: https://bitbucket.org/jawira/docker-wp-deploy/
- Docker Hub: https://hub.docker.com/r/jawira/wp-deploy/
